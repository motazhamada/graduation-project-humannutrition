package com.amg.humannutrition.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.amg.humannutrition.Adapters.FoodRecyclerViewAdapter;
import com.amg.humannutrition.Classes.ItemFood;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ChooseFoodActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static List<ItemFood> itemFood, itemFoodList;
    int temp = 0;
    private ConstraintLayout choosing_ConstraintLayout;
    private ProgressBar choosing_progressBar;
    private FoodRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private String Type_F_D_S;

    public static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_food);
        try {
            itemFood = new ArrayList<>();
            Button add_new_button_recyclerView = findViewById(R.id.add_new_button_recyclerView);
            Intent intent = getIntent();
            int category = intent.getIntExtra("Type", 1);
            if (category == 1) {
                Type_F_D_S = "D";//for drink
            } else if (category == 3) {
                Type_F_D_S = "S";//for suggested meal (drink,and food)
                add_new_button_recyclerView.setText("Suggest Another Meal");
            } else {
                Type_F_D_S = "F";//for food
            }
            choosing_ConstraintLayout = findViewById(R.id.choosing_ConstraintLayout);
            choosing_progressBar = findViewById(R.id.choosing_progressBar);
            add_new_button_recyclerView.setOnClickListener(v -> {
                if (Type_F_D_S.equals("S")) {
                    loadFood(Type_F_D_S);
                } else {
                      Intent intent1 = new Intent(ChooseFoodActivity.this, AddItemFCActivity.class);
                    intent1.putExtra("Type", Type_F_D_S);
                    startActivity(intent1);
                }
            });
            recyclerView = findViewById(R.id.choosing_recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            loadFood(Type_F_D_S);

            // set up the RecyclerView
            adapter = new FoodRecyclerViewAdapter(this, itemFood, choosing_ConstraintLayout);
            // adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseFoodActivity 1: " + e.toString());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadFood(Type_F_D_S);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.choose_food_menu, menu);
            final MenuItem searchItem = menu.findItem(R.id.action_search_food);
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(this);
            return true;
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseFoodActivity2: " + e.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if (!(query.trim().length() == 0)) {
            if (query.trim().length() > temp) {
                itemFood.clear();
                itemFood.addAll(itemFoodList);
                adapter.notifyDataSetChanged();
            }
            temp = query.trim().length();
            final List<ItemFood> filteredItemFood = new ArrayList<>();
            try {
                // //query number
                int numberQuery = Integer.parseInt(query);
                for (ItemFood mItemFood : itemFood) {
                    final int cal = Integer.valueOf(mItemFood.getCalories());
                    if (cal <= numberQuery) {
                        filteredItemFood.add(mItemFood);
                    }
                }
            } catch (NumberFormatException e) {
                if (isProbablyArabic(query)) {
                    //query arabic
                    for (ItemFood mItemFood : itemFood) {
                        final String text = mItemFood.getName_AR().toLowerCase();
                        if (text.contains(query)) {
                            filteredItemFood.add(mItemFood);
                        }
                    }
                } else {
                    //query english
                    query = query.toLowerCase();
                    for (ItemFood mItemFood : itemFood) {
                        final String text = mItemFood.getName_EN().toLowerCase();
                        if (text.contains(query)) {
                            filteredItemFood.add(mItemFood);
                        }
                    }
                }
            }
            adapter.animateTo(filteredItemFood);
            recyclerView.scrollToPosition(0);
        } else {
            itemFood.clear();
            itemFood.addAll(itemFoodList);
            adapter.notifyDataSetChanged();
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!(query.trim().length() == 0)) {
            final List<ItemFood> filteredItemFood = new ArrayList<>();
            try {
                // //query number
                int numberQuery = Integer.parseInt(query);
                for (ItemFood mItemFood : itemFood) {
                    final int cal = Integer.valueOf(mItemFood.getCalories());
                    if (cal <= numberQuery) {
                        filteredItemFood.add(mItemFood);
                    }
                }
            } catch (NumberFormatException e) {
                if (isProbablyArabic(query)) {
                    //query arabic
                    for (ItemFood mItemFood : itemFood) {
                        final String text = mItemFood.getName_AR();
                        if (text.contains(query)) {
                            filteredItemFood.add(mItemFood);
                        }
                    }

                } else {
                    //query english
                    query = query.toLowerCase();
                    for (ItemFood mItemFood : itemFood) {
                        final String text = mItemFood.getName_EN().toLowerCase();
                        if (text.contains(query)) {
                            filteredItemFood.add(mItemFood);
                        }
                    }
                }
            }
            adapter.animateTo(filteredItemFood);
            recyclerView.scrollToPosition(0);
        } else {
            itemFood.clear();
            itemFood.addAll(itemFoodList);
            adapter.notifyDataSetChanged();
        }

        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sort_eng_food:
                item.setChecked(true);
                sortByEngName();
                adapter.notifyDataSetChanged();
                return true;
            case R.id.sort_ar_food:
                item.setChecked(true);
                sortByArName();
                adapter.notifyDataSetChanged();
                return true;
            case R.id.sort_cal_food:
                item.setChecked(true);
                sortCalories();
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortByEngName() {
        Collections.sort(itemFood, (l1, l2) -> l1.getName_EN().compareTo(l2.getName_EN()));
    }

    private void sortByArName() {
        Collections.sort(itemFood, (l1, l2) -> l1.getName_AR().compareTo(l2.getName_AR()));
    }

    private void sortCalories() {
        Collections.sort(itemFood, (l1, l2) -> {
            if (Integer.valueOf(l1.getCalories()) > Integer.valueOf(l2.getCalories())) {
                return 1;
            } else if (Integer.valueOf(l1.getCalories()) < Integer.valueOf(l2.getCalories())) {
                return -1;
            } else {
                return 0;
            }
        });
    }


    void loadFood(final String Type) {
        // data to populate the RecyclerView with
        try {
            itemFood.clear();
            choosing_progressBar.setVisibility(View.VISIBLE);
            if(Type.equals("S"))
            {
                RequestQueue queue = Volley.newRequestQueue(ChooseFoodActivity.this);
                StringRequest request = new StringRequest(Request.Method.POST,
                        getString(R.string.Resource_Address) + getString(R.string.Suggest_Meal),
                        response -> {
                            if (String.valueOf(response) != null && !Objects.equals(response, "[]")) {
                                try {
                                    Gson gson = new Gson();
                                    itemFoodList = gson.fromJson(String.valueOf(response), new TypeToken<ArrayList<ItemFood>>() {
                                    }.getType());
                                    itemFood.addAll(itemFoodList);
                                    adapter.notifyDataSetChanged();
                                    choosing_progressBar.setVisibility(View.GONE);
                                } catch (Exception error) {
                                    choosing_progressBar.setVisibility(View.GONE);
                                    //If an error occurs that means end of the list has reached
                                    Snackbar snackbar = Snackbar
                                            .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    Log.e(getString(R.string.TAG), "ChooseFoodActivity3: " + error.toString());
                                    Log.e(getString(R.string.TAG), "ChooseFoodActivity response: " +response);

                                }
                            } else {
                                choosing_progressBar.setVisibility(View.GONE);
                                Snackbar snackbar = Snackbar
                                        .make(choosing_ConstraintLayout, "No More Items Available", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        }, error -> {
                    Snackbar snackbar = Snackbar
                            .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Calories", Get_User_Score().trim());
                        return params;
                    }
                };
                // Adding request to request queue
                queue.add(request);

            }
            // Instantiate the RequestQueue.
            else{
            RequestQueue queue = Volley.newRequestQueue(ChooseFoodActivity.this);
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Get_Food_Composition),
                    response -> {
                        if (response != null && !Objects.equals(response, "[]")) {
                            try {
                                Gson gson = new Gson();
                                itemFoodList = gson.fromJson(response, new TypeToken<ArrayList<ItemFood>>() {
                                }.getType());
                                itemFood.addAll(itemFoodList);
                                adapter.notifyDataSetChanged();
                                choosing_progressBar.setVisibility(View.GONE);
                            } catch (Exception error) {
                                choosing_progressBar.setVisibility(View.GONE);
                                //If an error occurs that means end of the list has reached
                                Snackbar snackbar = Snackbar
                                        .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                        } else {
                            choosing_progressBar.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(choosing_ConstraintLayout, "No More Items Available", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }, error -> {
                Snackbar snackbar = Snackbar
                        .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                snackbar.show();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Type", Type.trim());
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
            }

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseFoodActivity3: " + e.toString());
        }

    }

    private String Get_User_Score() {
        SharedPreferences sharedPref = getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserScore", null);
        String User_Score = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_Score = Objects.requireNonNull(jsonObject).getString("Score");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        if (Integer.valueOf(User_Score)<=600)
        {
            User_Score=String.valueOf(Integer.valueOf(User_Score)%600);
        }
            return User_Score;
        }
        return "";
    }
}
