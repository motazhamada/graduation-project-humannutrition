package com.amg.humannutrition.Activity;

import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amg.humannutrition.Fragment.ActivitiesHistoryFragment;
import com.amg.humannutrition.Fragment.GoalFragment;
import com.amg.humannutrition.Fragment.HomeFragment;
import com.amg.humannutrition.Fragment.NutritionHistoryFragment;
import com.amg.humannutrition.Fragment.SettingsFragment;
import com.amg.humannutrition.Fragment.ToolsFragment;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements OnDataPointListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_GOAL = "goal";
    private static final String TAG_NUTRITION_HISTORY = "nutrition_history";
    private static final String TAG_ACTIVITIES_HISTORY = "activities_history";
    private static final String TAG_TOOLS = "tools";
    private static final String TAG_SETTINGS = "settings";
    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    // index to identify current nav menu item
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_HOME;
    private RelativeLayout relativeLayoutMain;
    private FloatingActionMenu fab;
    private ProgressBar navigation_Progress;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    private Handler mHandler;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private boolean authInProgress = false;
    private GoogleApiClient mApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        relativeLayoutMain = findViewById(R.id.relativeLayoutMain);

        fab = findViewById(R.id.fab);
        FloatingActionButton fabWater = findViewById(R.id.fabWater);
        FloatingActionButton fabDrink = findViewById(R.id.fabDrink);
        FloatingActionButton fabMeal = findViewById(R.id.fabMeal);
        FloatingActionButton fabSuggestMeal = findViewById(R.id.fabSuggestMeal);
        FloatingActionButton fabActivity = findViewById(R.id.fabActivity);

        fab.setClosedOnTouchOutside(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        navigation_Progress = findViewById(R.id.navigation_Progress);

        fabWater.setOnClickListener(v -> {

            builder.setMessage("Do you want to add Cup of Water?")
                    .setTitle("Add Cup OF Water");
            builder.setIcon(R.drawable.ic_water_black_24dp);
            builder.setPositiveButton("OK", (dialog, id) -> {
                addCupOfWater();
                dialog.dismiss();
                fab.close(true);
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
                fab.close(true);
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });
        fabDrink.setOnClickListener(v -> {

            builder.setMessage("Do you want to add A Drink ?")
                    .setTitle("Add Drink");
            builder.setIcon(R.drawable.ic_drink_24dp);
            builder.setPositiveButton("OK", (dialog, id) -> {
                Intent intent = new Intent(MainActivity.this, ChooseFoodActivity.class);
                intent.putExtra("Type", 1);
                startActivity(intent);
                dialog.dismiss();
                fab.close(true);
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
                fab.close(true);
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        fabMeal.setOnClickListener(v -> {
            builder.setMessage("Do you want to add A Meal ?")
                    .setTitle("Add Meal");
            builder.setIcon(R.drawable.ic_local_meal_24dp);
            builder.setPositiveButton("OK", (dialog, id) -> {
                Intent intent = new Intent(MainActivity.this, ChooseFoodActivity.class);
                intent.putExtra("Type", 2);
                startActivity(intent);
                dialog.dismiss();
                fab.close(true);
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
                fab.close(true);
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        fabSuggestMeal.setOnClickListener(v -> {

            builder.setMessage("Do you want to A Suggested Meal ?")
                    .setTitle("Add Meal");
            builder.setIcon(R.drawable.ic_local_meal_24dp);
            builder.setPositiveButton("OK", (dialog, id) -> {
                Intent intent = new Intent(MainActivity.this, ChooseFoodActivity.class);
                intent.putExtra("Type", 3);
                startActivity(intent);
                dialog.dismiss();
                fab.close(true);
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
                fab.close(true);
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        fabActivity.setOnClickListener(v -> {
            builder.setMessage("Do you want to add An Activity ?")
                    .setTitle("Add Activity");
            builder.setIcon(R.drawable.ic_directions_run_black_24dp);
            builder.setPositiveButton("OK", (dialog, id) -> {
                Intent intent = new Intent(MainActivity.this, ChooseActivityActivity.class);
                startActivity(intent);
                dialog.dismiss();
                fab.close(true);
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
                fab.close(true);
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        // Navigation view header
        View navHeader = navigationView.getHeaderView(0);
        TextView txtName = navHeader.findViewById(R.id.header_name);
        TextView txtEmail = navHeader.findViewById(R.id.header_email);
        //ImageView imgProfile = navHeader.findViewById(R.id.header_imageprofile);
        String userJsonData = Get_UserData();
        if (Objects.equals(userJsonData, "null")) {
            logout();
        } else {
            try {
                JSONObject jsonObject = new JSONObject(userJsonData);
                String headerName = jsonObject.getString("Username");
                String headerEmail = jsonObject.getString("Email");
                //String Email = jsonObject.getString("Email");
                txtName.setText(headerName);
                txtEmail.setText(headerEmail);
            } catch (Exception e) {
                Log.e(getString(R.string.TAG), "Error: " + e.toString());
            }

        }

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        // flag to load home fragment when user presses back key
        // checking if user is on other navigation menu
        // rather than home
        if (navItemIndex != 0) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
            return;
        }

        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    void Set_EmailData() {
        try {
            SharedPreferences.Editor sharedPref = getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.clear();
            sharedPref.apply();
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "Error: " + e.toString());
        }
    }


    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = () -> {
            // update the main content by replacing fragments
            Fragment fragment = getHomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.container, fragment, CURRENT_TAG);
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        mHandler.post(mPendingRunnable);


        //Closing drawer on item click
        drawer.closeDrawer(GravityCompat.START);
        fab.close(true);
        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                return new HomeFragment();
            case 1:
                // photos
                return new GoalFragment();
            case 2:
                // movies fragment
                return new NutritionHistoryFragment();
            case 3:
                // notifications fragment
                return new ActivitiesHistoryFragment();

            case 4:
                // notifications fragment
                return new ToolsFragment();

            case 5:
                // settings fragment
                return new SettingsFragment();
            default:
                return new HomeFragment();
        }
    }

    private void setToolbarTitle() {
        Objects.requireNonNull(getSupportActionBar()).setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {

        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        // This method will trigger on item Click of navigation menu
        navigationView.setNavigationItemSelectedListener(item -> {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            if (id == R.id.nav_logout) {
                logout();
            } else {
                if (id == R.id.nav_home) {
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_HOME;
                } else if (id == R.id.nav_goal) {
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_GOAL;
                } else if (id == R.id.nav_nutrition) {
                    navItemIndex = 2;
                    CURRENT_TAG = TAG_NUTRITION_HISTORY;
                } else if (id == R.id.nav_activities) {
                    navItemIndex = 3;
                    CURRENT_TAG = TAG_ACTIVITIES_HISTORY;
                } else if (id == R.id.nav_tools) {
                    navItemIndex = 4;
                    CURRENT_TAG = TAG_TOOLS;
                } else if (id == R.id.nav_settings) {
                    navItemIndex = 5;
                    CURRENT_TAG = TAG_SETTINGS;
                }
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                loadHomeFragment();
            }  //Checking if the item is in checked state or not, if not make it in checked state
            if (item.isChecked()) {
                item.setChecked(false);
            } else {
                item.setChecked(true);
            }
            item.setChecked(true);

            return true;
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                fab.setEnabled(true);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

                fab.setEnabled(false);
                fab.close(true);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void logout() {
        try {
            navigation_Progress.setVisibility(View.VISIBLE);
            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            StringRequest tokenRequest = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.UpdateToken_Address),
                    response -> {
                        if (Objects.equals(String.valueOf(response), "Done")) {
                            Set_EmailData();
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            navigation_Progress.setVisibility(View.GONE);
                            Log.e(getString(R.string.TAG), "Error: response =" + String.valueOf(response));
                        }
                    }, error -> {
                navigation_Progress.setVisibility(View.GONE);
                //do nothing
                Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", Get_Email());
                    params.put("token", "1");
                    return params;
                }

            };
            // Adding request to request queue
            queue.add(tokenRequest);
            Fitness.SensorsApi.remove(mApiClient, this)
                    .setResultCallback(status -> {
                        if (status.isSuccess()) {
                            mApiClient.disconnect();
                        }
                    });
        } catch (Exception e) {
            navigation_Progress.setVisibility(View.GONE);
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }

    }

    String Get_UserData() {
        SharedPreferences sharedPref = getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserData", null);
        if (restoredText != null) {
//            String Name = sharedPref.getString("name", "No name defined");//"No name defined" is the default value.
//            String Email = sharedPref.getString("name", "No name defined");//"No name defined" is the default value.
            return restoredText;
        }
        return "null";
    }

    String Get_Email() {
        SharedPreferences sharedPref = getSharedPreferences("UserData", MODE_PRIVATE);
        String Email = sharedPref.getString("Email", null);
        if (Email != null) {
            return Email;
        }
        return "";
    }

    @Override
    protected void onStart() {
        super.onStart();
        mApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();

        ResultCallback<DataSourcesResult> dataSourcesResultCallback = dataSourcesResult -> {
            for (DataSource dataSource : dataSourcesResult.getDataSources()) {
                if (DataType.TYPE_STEP_COUNT_CUMULATIVE.equals(dataSource.getDataType())) {
                    registerFitnessDataListener(dataSource);
                }
            }
        };

        Fitness.SensorsApi.findDataSources(mApiClient, dataSourceRequest)
                .setResultCallback(dataSourcesResultCallback);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!authInProgress) {
            try {
                authInProgress = true;
                connectionResult.startResolutionForResult(MainActivity.this, REQUEST_OAUTH);
            } catch (IntentSender.SendIntentException ignored) {
            }
        } else {
            Log.e("GoogleFit", "authInProgress");
        }
    }

    @Override
    public void onDataPoint(DataPoint dataPoint) {
        for (final Field field : dataPoint.getDataType().getFields()) {
            final Value value = dataPoint.getValue(field);
            runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_OAUTH) {
            authInProgress = false;
            if (resultCode == RESULT_OK) {
                if (!mApiClient.isConnecting() && !mApiClient.isConnected()) {
                    mApiClient.connect();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("GoogleFit", "RESULT_CANCELED");
            }
        } else {
            Log.e("GoogleFit", "requestCode NOT request_oauth");
        }
    }

    private void registerFitnessDataListener(DataSource dataSource) {

        SensorRequest request = new SensorRequest.Builder()
                .setDataSource(dataSource)
                .setDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setSamplingRate(3, TimeUnit.SECONDS)
                .build();

        Fitness.SensorsApi.add(mApiClient, request, this)
                .setResultCallback(status -> {
                    if (status.isSuccess()) {
                        Log.e("GoogleFit", "SensorApi successfully added");
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fitness.SensorsApi.remove(mApiClient, this)
                .setResultCallback(status -> {
                    if (status.isSuccess()) {
                        mApiClient.disconnect();
                    }
                });
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        Fitness.SensorsApi.remove( mApiClient, this )
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        if (status.isSuccess()) {
//                            mApiClient.disconnect();
//                        }
//                    }
//                });
//    }

    private void addCupOfWater() {
        try {
            RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            StringRequest tokenRequest = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Add_Cup_OF_Water),
                    response -> {
                        if (Objects.equals(response, "Added") || Objects.equals(response, "Updated")) {
                            Snackbar snackbar = Snackbar.make(relativeLayoutMain, "Great. Water is very important to human life", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                            Snackbar snackbar = Snackbar.make(relativeLayoutMain, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            Log.e(getString(R.string.TAG), "Error: response =" + response);
                        }
                    }, error -> {
                Snackbar snackbar = Snackbar.make(relativeLayoutMain, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                snackbar.show();
                //do nothing
                Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Email", Get_Email());
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(tokenRequest);

        } catch (Exception e) {
            Snackbar snackbar = Snackbar.make(relativeLayoutMain, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_PENDING, authInProgress);
    }
}

