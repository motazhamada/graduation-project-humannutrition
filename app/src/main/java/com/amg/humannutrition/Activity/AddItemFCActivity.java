package com.amg.humannutrition.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddItemFCActivity extends AppCompatActivity {
    String Type_F_D;
    ConstraintLayout adding_new_item_constraintLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item_fc);
        Intent intent = getIntent();
        Type_F_D = intent.getStringExtra("Type");

        adding_new_item_constraintLayout = findViewById(R.id.adding_new_item_constraintLayout);
        Button cancel_action_add_new_dialog = findViewById(R.id.cancel_action_add_new_dialog);
        Button choose_action_add_new_dialog = findViewById(R.id.choose_action_add_new_dialog);
        NumberPicker calories_numberPicker_add_new_dialog = findViewById(R.id.calories_numberPicker_add_new_dialog);
        NumberPicker carbs_numberPicker_add_new_dialog = findViewById(R.id.carbs_numberPicker_add_new_dialog);
        NumberPicker protein_numberPicker_add_new_dialog = findViewById(R.id.protein_numberPicker_add_new_dialog);
        NumberPicker fat_numberPicker_add_new_dialog = findViewById(R.id.fat_numberPicker_add_new_dialog);
        EditText name_eng_add_new_dialog = findViewById(R.id.name_eng_add_new_dialog);
        EditText name_ar_add_new_dialog = findViewById(R.id.name_ar_add_new_dialog);

        calories_numberPicker_add_new_dialog.setMinValue(1);
        calories_numberPicker_add_new_dialog.setMaxValue(1000);

        carbs_numberPicker_add_new_dialog.setMinValue(1);
        carbs_numberPicker_add_new_dialog.setMaxValue(500);

        protein_numberPicker_add_new_dialog.setMinValue(1);
        protein_numberPicker_add_new_dialog.setMaxValue(500);

        fat_numberPicker_add_new_dialog.setMinValue(1);
        fat_numberPicker_add_new_dialog.setMaxValue(500);


        cancel_action_add_new_dialog.setOnClickListener(v -> {
            try {
                onBackPressed();
            } catch (Exception e) {
                Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
            }
        });
        choose_action_add_new_dialog.setOnClickListener(v -> {
            try {
                if (!Objects.equals(name_eng_add_new_dialog.getText().toString().trim(), "") && !Objects.equals(name_ar_add_new_dialog.getText().toString().trim(), "")) {
                    RequestQueue queue = Volley.newRequestQueue(AddItemFCActivity.this);
                    StringRequest tokenRequest = new StringRequest(Request.Method.POST,
                            getString(R.string.Resource_Address) + getString(R.string.Add_Food_Composition),
                            response -> {
                                if (Objects.equals(response, "Added")) {
                                    Snackbar snackbar = Snackbar
                                            .make(adding_new_item_constraintLayout, "Added Successfully", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    onBackPressed();
                                } else {
                                    Log.e(getString(R.string.TAG), "Error: response =" + response);
                                    Snackbar snackbar = Snackbar
                                            .make(adding_new_item_constraintLayout, "Sorry this item has not been Added", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    onBackPressed();
                                }
                            }, error -> {
                        //do nothing
                        Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Type", Type_F_D);
                            params.put("Name_EN", name_eng_add_new_dialog.getText().toString().trim());
                            params.put("Name_AR", name_ar_add_new_dialog.getText().toString().trim());
                            params.put("Calories", String.valueOf(calories_numberPicker_add_new_dialog.getValue()));
                            params.put("Carbs", String.valueOf(carbs_numberPicker_add_new_dialog.getValue()));
                            params.put("Protein", String.valueOf(protein_numberPicker_add_new_dialog.getValue()));
                            params.put("Fats", String.valueOf(fat_numberPicker_add_new_dialog.getValue()));
                            params.put("Food_GP_ID", "0");
                            params.put("User_ID", Get_User_ID());
                            return params;
                        }
                    };
                    // Adding request to request queue
                    queue.add(tokenRequest);

                }
                else {
                    Snackbar snackbar = Snackbar
                            .make(adding_new_item_constraintLayout, "sorry you must fill all the data", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
                } catch(Exception e){
                    Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
                }
            });

    }

    String Get_User_ID() {
        SharedPreferences sharedPref = getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String User_ID = "";
        if (JsonUserData != null) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_ID = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return User_ID;
    }

}
