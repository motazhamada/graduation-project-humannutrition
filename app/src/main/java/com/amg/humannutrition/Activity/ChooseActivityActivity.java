package com.amg.humannutrition.Activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.amg.humannutrition.Adapters.ActivityRecyclerViewAdapter;
import com.amg.humannutrition.Classes.ItemActivity;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ChooseActivityActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static List<ItemActivity> itemActivity, itemActivityList;
    int temp = 0;
    private ConstraintLayout choosing_ConstraintLayout;
    private ProgressBar choosing_progressBar;
    private ActivityRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private AlertDialog alertDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_food);
        try {
            itemActivity = new ArrayList<>();
            choosing_ConstraintLayout = findViewById(R.id.choosing_ConstraintLayout);
            choosing_progressBar = findViewById(R.id.choosing_progressBar);
            Button add_new_button_recyclerView = findViewById(R.id.add_new_button_recyclerView);
            add_new_button_recyclerView.setVisibility(View.GONE);
//            add_new_button_recyclerView.setOnClickListener(v -> {
//                Intent intent=new Intent(ChooseActivityActivity.this,AddItemFCActivity.class);
//                startActivity(intent);
//            });

            recyclerView = findViewById(R.id.choosing_recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            // set up the RecyclerView
            adapter = new ActivityRecyclerViewAdapter(this, itemActivity, choosing_ConstraintLayout);
            // adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseActivityActivity1: " + e.toString());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadActivities();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.choose_activity_menu, menu);
            final MenuItem searchItem = menu.findItem(R.id.action_search_activity);
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchView.setOnQueryTextListener(this);

            return true;
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseActivityActivity2: " + e.toString());
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if (!(query.trim().length() == 0)) {
            if (query.trim().length() > temp) {
                itemActivity.clear();
                itemActivity.addAll(itemActivityList);
                adapter.notifyDataSetChanged();
            }
            temp = query.trim().length();
            final List<ItemActivity> filteredItemActivity = new ArrayList<>();
            try {
                // //query number
                int numberQuery = Integer.parseInt(query);
                for (ItemActivity mItemActivity : itemActivity) {
                    final int cal = Integer.valueOf(mItemActivity.getCalories_Burned());
                    if (cal <= numberQuery) {
                        filteredItemActivity.add(mItemActivity);
                    }
                }
            } catch (NumberFormatException e) {
                //query english
                query = query.toLowerCase();
                for (ItemActivity mItemActivity : itemActivity) {
                    final String text = mItemActivity.getActicity_Name().toLowerCase();
                    if (text.contains(query)) {
                        filteredItemActivity.add(mItemActivity);
                    }
                }
            }
            adapter.animateTo(filteredItemActivity);
            recyclerView.scrollToPosition(0);
        } else {
            itemActivity.clear();
            itemActivity.addAll(itemActivityList);
            adapter.notifyDataSetChanged();
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (!(query.trim().length() == 0)) {
            if (query.trim().length() > temp) {
                itemActivity.clear();
                itemActivity.addAll(itemActivityList);
                adapter.notifyDataSetChanged();
            }
            temp = query.trim().length();
            final List<ItemActivity> filteredItemActivity = new ArrayList<>();
            try {
                // //query number
                int numberQuery = Integer.parseInt(query);
                for (ItemActivity mItemActivity : itemActivity) {
                    final int cal = Integer.valueOf(mItemActivity.getCalories_Burned());
                    if (cal <= numberQuery) {
                        filteredItemActivity.add(mItemActivity);
                    }
                }
            } catch (NumberFormatException e) {
                //query english
                query = query.toLowerCase();
                for (ItemActivity mItemActivity : itemActivity) {
                    final String text = mItemActivity.getActicity_Name().toLowerCase();
                    if (text.contains(query)) {
                        filteredItemActivity.add(mItemActivity);
                    }
                }
            }
            adapter.animateTo(filteredItemActivity);
            recyclerView.scrollToPosition(0);
        } else {
            itemActivity.clear();
            itemActivity.addAll(itemActivityList);
            adapter.notifyDataSetChanged();
        }
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sort_name_activity:
                item.setChecked(true);
                sortByEngName();
                adapter.notifyDataSetChanged();
                return true;
            case R.id.sort_cal_activity:
                item.setChecked(true);
                sortCalories();
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortByEngName() {
        Collections.sort(itemActivity, (l1, l2) -> l1.getActicity_Name().compareTo(l2.getActicity_Name()));
    }

    private void sortCalories() {
        Collections.sort(itemActivity, (l1, l2) -> {
            if (Integer.valueOf(l1.getCalories_Burned()) > Integer.valueOf(l2.getCalories_Burned())) {
                return 1;
            } else if (Integer.valueOf(l1.getCalories_Burned()) < Integer.valueOf(l2.getCalories_Burned())) {
                return -1;
            } else {
                return 0;
            }
        });
    }


    void loadActivities() {
        // data to populate the RecyclerView with
        try {
            itemActivity.clear();
            choosing_progressBar.setVisibility(View.VISIBLE);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(ChooseActivityActivity.this);
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Get_All_Activities),
                    response -> {
                        if (response != null && !Objects.equals(response, "[]")) {
                            try {
                                Gson gson = new Gson();
                                itemActivityList = gson.fromJson(response, new TypeToken<ArrayList<ItemActivity>>() {
                                }.getType());

                                itemActivity.addAll(itemActivityList);
                                adapter.notifyDataSetChanged();
                                choosing_progressBar.setVisibility(View.GONE);
                            } catch (Exception error) {
                                choosing_progressBar.setVisibility(View.GONE);
                                //If an error occurs that means end of the list has reached
                                Snackbar snackbar = Snackbar
                                        .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                                snackbar.show();
                                loadActivities();

                                Log.e(getString(R.string.TAG), "ChooseActivityActivity13: " + error.toString());
                            }
                        } else {
                            choosing_progressBar.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(choosing_ConstraintLayout, "No More Items Available", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }
                    }, error -> {
                Snackbar snackbar = Snackbar
                        .make(choosing_ConstraintLayout, "sorry there was an error with downloading data from server", Snackbar.LENGTH_LONG);
                snackbar.show();
            }) {
                @Override
                protected Map<String, String> getParams() {
                    return new HashMap<>();
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "ChooseActivityActivity3: " + e.toString());
        }
    }
}

