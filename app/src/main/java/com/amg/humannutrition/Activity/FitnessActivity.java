package com.amg.humannutrition.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.amg.humannutrition.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.result.DataReadResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FitnessActivity extends AppCompatActivity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness);



//        FitnessOptions fitnessOptions = FitnessOptions.builder()
//                .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
//                .addDataType(DataType.AGGREGATE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
//                .build();
//
//
//
////        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
////            GoogleSignIn.requestPermissions(
////                    this, // your activity
////                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
////                    GoogleSignIn.getLastSignedInAccount(this),
////                    fitnessOptions);
////        } else {
////            accessGoogleFit();
////        }
////    }
////    @Override
////    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////        if (resultCode == Activity.RESULT_OK) {
////            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
////                accessGoogleFit();
////            }
////        }
////    }
////    private void accessGoogleFit() {
////        Calendar cal = Calendar.getInstance();
////        cal.setTime(new Date());
////        long endTime = cal.getTimeInMillis();
////        cal.add(Calendar.YEAR, -1);
////        long startTime = cal.getTimeInMillis();
////
////
////        DataReadRequest readRequest = new DataReadRequest.Builder()
////                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
////                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
////                .build();
////
////        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
////                .readData(readRequest)
////                .addOnSuccessListener(new OnSuccessListener() {
////                    @Override
////                    public void onSuccess(DataReadResponse dataReadResponse) {
////                        Log.d(LOG_TAG, "onSuccess()");
////                    }
////                })
////                .addOnFailureListener(new OnFailureListener() {
////                    @Override
////                    public void onFailure(@NonNull Exception e) {
////                        Log.e(LOG_TAG, "onFailure()", e);
////                    }
////                })
////                .addOnCompleteListener(new OnCompleteListener() {
////                    @Override
////                    public void onComplete(@NonNull Task task) {
////                        Log.d(LOG_TAG, "onComplete()");
////                    }
////                });
    }



}
