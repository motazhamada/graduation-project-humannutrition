package com.amg.humannutrition.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.amg.humannutrition.Classes.ItemActivity;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static com.amg.humannutrition.R.layout.custom_dialog_activity_choose;

public class ActivityRecyclerViewAdapter extends RecyclerView.Adapter<ActivityRecyclerViewAdapter.ViewHolder> implements
        android.view.View.OnClickListener {

    private AlertDialog alertDialog = null;
    private List<ItemActivity> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private View mView;

    // data is passed into the constructor
    public ActivityRecyclerViewAdapter(Context context, List<ItemActivity> data, View view) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mView = view;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_activity_recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    public void add(int position, ItemActivity item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void animateTo(List<ItemActivity> item) {
        applyAndAnimateRemovals(item);
        applyAndAnimateAdditions(item);
        applyAndAnimateMovedItems(item);
    }

    private void applyAndAnimateRemovals(List<ItemActivity> newModels) {
        for (int i = mData.size() - 1; i >= 0; i--) {
            final ItemActivity model = mData.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<ItemActivity> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final ItemActivity model = newModels.get(i);
            if (!mData.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<ItemActivity> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final ItemActivity model = newModels.get(toPosition);
            final int fromPosition = mData.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public ItemActivity removeItem(int position) {
        final ItemActivity model = mData.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, ItemActivity model) {
        mData.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final ItemActivity model = mData.remove(fromPosition);
        mData.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void remove(ItemActivity item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemActivity ItemActivity = mData.get(position);
        try {

            holder.activity_name_textView.setText(ItemActivity.getActicity_Name());
            holder.activity_calories_textView.setText(String.format("%s kcal", ItemActivity.getCalories_Burned()));
            holder.item_Ripple_activity_row.setOnClickListener(v -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                // Set icon value.
                builder.setIcon(R.drawable.ic_directions_run_black_24dp);
                // Set title value.
                builder.setTitle("Add Activity");
                View customFormView = View.inflate(mContext, custom_dialog_activity_choose, null);
                builder.setView(customFormView);


                TextView message = customFormView.findViewById(R.id.textView_Dialog);
                Button cancel_action_Dialog = customFormView.findViewById(R.id.cancel_action_Dialog);
                Button choose_action_Dialog = customFormView.findViewById(R.id.choose_action_dialog);
                NumberPicker numberPicker_Dialog = customFormView.findViewById(R.id.numberPicker_Dialog);
                message.setText("How long have you spent practicing this activity (in min)?");
                numberPicker_Dialog.setMinValue(10);
                numberPicker_Dialog.setMaxValue(1000);
                cancel_action_Dialog.setOnClickListener(v1 -> alertDialog.cancel());
                choose_action_Dialog.setOnClickListener(v12 ->
                {
                    double Calories_Burned_temp = (Double.valueOf(ItemActivity.getCalories_Burned()) / 10) * numberPicker_Dialog.getValue();
                    addUserActivity(ItemActivity.getActivities_ID(), String.valueOf(Calories_Burned_temp));
                    alertDialog.dismiss();
                });
                builder.setCancelable(true);
                alertDialog = builder.create();
                alertDialog.show();
            });
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), " ActivityRecyclerViewAdapter 1 = " + String.valueOf(e));
        }
    }

    public void addUserActivity(String Activity_ID, String Calories_Burned) {
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest tokenRequest = new StringRequest(Request.Method.POST,
                    mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Add_User_Activity),
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                String sResponse = String.valueOf(response);
                                //JSONObject jsonObject = new JSONObject(sResponse);
                                Set_UserScore(sResponse);
                                if (Check_UserScore()) {
                                    Snackbar snackbar = Snackbar.make(mView, "Nice Job. Activity added successfully", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } else {
                                    Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    Log.e(mContext.getString(R.string.TAG), "Error: response =" + response);
                                }
                            } catch (Exception e) {
                                Log.e(mContext.getString(R.string.TAG), "Error: response =" + e.getMessage());
                            }
                        }
                    }, error -> {
                Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                snackbar.show();
                //do nothing
                Log.e(mContext.getString(R.string.TAG), "ActivityRecyclerViewAdapter 2: " + error.getMessage());
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("User_ID", Get_User_ID());
                    params.put("Activity_ID", Activity_ID);
                    params.put("Calories_Burned", Calories_Burned);
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(tokenRequest);

        } catch (Exception e) {
            Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(mContext.getString(R.string.TAG), " ActivityRecyclerViewAdapter 3 = " + String.valueOf(e));
        }
    }

    String Get_User_ID() {
        SharedPreferences sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String User_ID = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_ID = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return User_ID;
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public ItemActivity getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialRippleLayout item_Ripple_activity_row;
        TextView activity_name_textView, activity_calories_textView;

        ViewHolder(View itemView) {
            super(itemView);
            item_Ripple_activity_row = itemView.findViewById(R.id.item_Ripple_activity_row);
            activity_name_textView = itemView.findViewById(R.id.activity_name_textView);
            activity_calories_textView = itemView.findViewById(R.id.activity_calories_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    private void Set_UserScore(String UserScore) {
        try {
            SharedPreferences.Editor sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.putString("JsonUserScore", UserScore);
            sharedPref.apply();
            sharedPref.commit();
        }
        catch (Exception e){
//        Log.e(getString(R.string.TAG), "Error: "+e.toString());
        }}

    private boolean Check_UserScore() {
        SharedPreferences sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserScore", null);
        return restoredText != null;
    }


}
