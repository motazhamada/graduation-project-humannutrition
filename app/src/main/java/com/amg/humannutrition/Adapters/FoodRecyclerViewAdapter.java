package com.amg.humannutrition.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.amg.humannutrition.Classes.ItemFood;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static com.amg.humannutrition.R.layout.custom_dialog_activity_choose;

public class FoodRecyclerViewAdapter extends RecyclerView.Adapter<FoodRecyclerViewAdapter.ViewHolder> implements
        android.view.View.OnClickListener {

    private AlertDialog alertDialog = null;
    private List<ItemFood> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private View mView;

    // data is passed into the constructor
    public FoodRecyclerViewAdapter(Context context, List<ItemFood> data, View view) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mView = view;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_food_recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    public void add(int position, ItemFood item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void animateTo(List<ItemFood> item) {
        applyAndAnimateRemovals(item);
        applyAndAnimateAdditions(item);
        applyAndAnimateMovedItems(item);
    }

    private void applyAndAnimateRemovals(List<ItemFood> newModels) {
        for (int i = mData.size() - 1; i >= 0; i--) {
            final ItemFood model = mData.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<ItemFood> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final ItemFood model = newModels.get(i);
            if (!mData.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<ItemFood> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final ItemFood model = newModels.get(toPosition);
            final int fromPosition = mData.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public ItemFood removeItem(int position) {
        final ItemFood model = mData.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, ItemFood model) {
        mData.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final ItemFood model = mData.remove(fromPosition);
        mData.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void remove(ItemFood item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemFood ItemFood = mData.get(position);
        try {

            holder.foodName_EN_textView.setText(ItemFood.getName_EN());
            holder.foodName_AR_textView.setText(ItemFood.getName_AR());
            holder.foodCalories_textView.setText(String.format("%s kcal", ItemFood.getCalories()));
            holder.item_Ripple_food_row.setOnClickListener(v -> {
                try {

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    // Set icon value.
                    builder.setIcon(R.drawable.ic_drink_24dp);

                    // Set title value.
                    builder.setTitle("Add New Item");
                    //View view = new View(mContext);
                    View customFormView = View.inflate(mContext, custom_dialog_activity_choose, null);
                    builder.setView(customFormView);

                    TextView message = customFormView.findViewById(R.id.textView_Dialog);
                    Button cancel_action_Dialog = customFormView.findViewById(R.id.cancel_action_Dialog);
                    Button choose_action_Dialog = customFormView.findViewById(R.id.choose_action_dialog);
                    NumberPicker numberPicker_Dialog = customFormView.findViewById(R.id.numberPicker_Dialog);
                    message.setText("what is the weight of the item in grams?");
                    numberPicker_Dialog.setMinValue(50);
                    numberPicker_Dialog.setMaxValue(1000);
                    cancel_action_Dialog.setOnClickListener(v1 -> alertDialog.cancel());
                    choose_action_Dialog.setOnClickListener(v12 ->
                    {
                        String Calories = String.valueOf((Double.valueOf(ItemFood.getCalories()) / 100) * numberPicker_Dialog.getValue());
                        addEatenMeal(ItemFood.getFC_ID(), String.valueOf(numberPicker_Dialog.getValue()), Calories);
                        alertDialog.dismiss();
                    });
                    builder.setCancelable(true);
                    alertDialog = builder.create();
                    alertDialog.show();

                } catch (Exception e) {
                    Log.e(mContext.getString(R.string.TAG), " FoodRecyclerViewAdapter 11 = " + String.valueOf(e));
                }
            });
        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), " FoodRecyclerViewAdapter1 = " + String.valueOf(e));
        }
    }

    public void addEatenMeal(String FC_ID, String Weight, String Calories_Burned) {
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest tokenRequest = new StringRequest(Request.Method.POST,
                    mContext.getString(R.string.Resource_Address) + mContext.getString(R.string.Add_Eaten_Meal),
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                String sResponse = String.valueOf(response);
                                //JSONObject jsonObject = new JSONObject(sResponse);
                                Set_UserScore(sResponse);
                                if (Check_UserScore()) {
                                    Snackbar snackbar = Snackbar.make(mView, "Great. item added successfully", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                } else {
                                    Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    Log.e(mContext.getString(R.string.TAG), "Error: response =" + response);
                                }
                            } catch (Exception e) {
                                Log.e(mContext.getString(R.string.TAG), "Error: response =" + e.getMessage());
                            }
                        }
                    }, error -> {
                Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
                snackbar.show();
                //do nothing
                Log.e(mContext.getString(R.string.TAG), "FoodRecyclerViewAdapter 2: " + error.getMessage());
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("User_ID", Get_User_ID());
                    params.put("FC_ID", FC_ID);
                    params.put("Weight", Weight);
                    params.put("Calories_Burned", Calories_Burned);
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(tokenRequest);

        } catch (Exception e) {
            Snackbar snackbar = Snackbar.make(mView, "sorry there was an Error Check your connection Or call The admin", Snackbar.LENGTH_LONG);
            snackbar.show();
            Log.e(mContext.getString(R.string.TAG), " FoodRecyclerViewAdapter3 = " + String.valueOf(e));
        }

    }

    private String Get_User_ID() {
        SharedPreferences sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String User_ID = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_ID = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return User_ID;
        }
        return "";
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public ItemFood getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialRippleLayout item_Ripple_food_row;
        TextView foodName_EN_textView, foodName_AR_textView, foodCalories_textView;

        ViewHolder(View itemView) {
            super(itemView);
            item_Ripple_food_row = itemView.findViewById(R.id.item_Ripple_food_row);
            foodName_EN_textView = itemView.findViewById(R.id.foodName_EN_textView);
            foodName_AR_textView = itemView.findViewById(R.id.foodName_AR_textView);
            foodCalories_textView = itemView.findViewById(R.id.foodCalories_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    private void Set_UserScore(String UserScore) {
        try {
            SharedPreferences.Editor sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.putString("JsonUserScore", UserScore);
            sharedPref.apply();
            sharedPref.commit();
        }
        catch (Exception e){
//        Log.e(getString(R.string.TAG), "Error: "+e.toString());
        }}

    private boolean Check_UserScore() {
        SharedPreferences sharedPref = mContext.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserScore", null);
        return restoredText != null;
    }

}