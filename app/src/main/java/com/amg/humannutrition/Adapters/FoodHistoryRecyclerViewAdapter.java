package com.amg.humannutrition.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amg.humannutrition.Classes.ItemEatenMeal;
import com.amg.humannutrition.R;
import com.balysv.materialripple.MaterialRippleLayout;

import java.util.List;

public class FoodHistoryRecyclerViewAdapter extends RecyclerView.Adapter<FoodHistoryRecyclerViewAdapter.ViewHolder> implements
        android.view.View.OnClickListener {

    private List<ItemEatenMeal> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;


    // data is passed into the constructor
    public FoodHistoryRecyclerViewAdapter(Context context, List<ItemEatenMeal> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_food_history_recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    public void add(int position, ItemEatenMeal item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void animateTo(List<ItemEatenMeal> item) {
        applyAndAnimateRemovals(item);
        applyAndAnimateAdditions(item);
        applyAndAnimateMovedItems(item);
    }

    private void applyAndAnimateRemovals(List<ItemEatenMeal> newModels) {
        for (int i = mData.size() - 1; i >= 0; i--) {
            final ItemEatenMeal model = mData.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<ItemEatenMeal> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final ItemEatenMeal model = newModels.get(i);
            if (!mData.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<ItemEatenMeal> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final ItemEatenMeal model = newModels.get(toPosition);
            final int fromPosition = mData.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public ItemEatenMeal removeItem(int position) {
        final ItemEatenMeal model = mData.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, ItemEatenMeal model) {
        mData.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final ItemEatenMeal model = mData.remove(fromPosition);
        mData.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void remove(ItemEatenMeal item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        MaterialRippleLayout item_Ripple_food_history_row;
        TextView foodName_EN_history_textView, foodName_AR_history_textView, foodCalories_history_textView, date_time_history_textView;

        ViewHolder(View itemView) {
            super(itemView);
            item_Ripple_food_history_row =  itemView.findViewById(R.id.item_Ripple_food_history_row);
            foodName_EN_history_textView = itemView.findViewById(R.id.foodName_EN_history_textView);
            foodName_AR_history_textView = itemView.findViewById(R.id.foodName_AR_history_textView);
            foodCalories_history_textView = itemView.findViewById(R.id.foodCalories_history_textView);
            date_time_history_textView = itemView.findViewById(R.id.date_time_history_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemEatenMeal ItemEatenMeal = mData.get(position);
        try {
            holder.foodName_EN_history_textView.setText(ItemEatenMeal.getName_EN());
            holder.foodName_AR_history_textView.setText(ItemEatenMeal.getName_AR());
            holder.foodCalories_history_textView.setText(String.format("%s kcal", ItemEatenMeal.getCalories()));
            holder.date_time_history_textView.setText(ItemEatenMeal.getDate_Time());

        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), " FoodHistoryRecyclerViewAdpter 1 = " + String.valueOf(e));
        }
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public ItemEatenMeal getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}