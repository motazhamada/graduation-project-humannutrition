package com.amg.humannutrition.Classes;

public class ItemEatenMeal {

    private String EM_ID;
    private String User_ID;
    private String FC_ID;
    private String Type;
    private String Name_EN;
    private String Carbs;
    private String Protein;
    private String Fats;
    private String Name_AR;
    private String Calories;
    private String Food_GP_ID;
    private String Food_GP_Name;
    private String Weight;
    private String Date_Time;

    public String getEM_ID() {
        return EM_ID;
    }

    public void setEM_ID(String EM_ID) {
        this.EM_ID = EM_ID;
    }

    public String getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(String user_ID) {
        User_ID = user_ID;
    }

    public String getFC_ID() {
        return FC_ID;
    }

    public void setFC_ID(String FC_ID) {
        this.FC_ID = FC_ID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName_EN() {
        return Name_EN;
    }

    public void setName_EN(String name_EN) {
        Name_EN = name_EN;
    }

    public String getCarbs() {
        return Carbs;
    }

    public void setCarbs(String carbs) {
        Carbs = carbs;
    }

    public String getProtein() {
        return Protein;
    }

    public void setProtein(String protein) {
        Protein = protein;
    }

    public String getFats() {
        return Fats;
    }

    public void setFats(String fats) {
        Fats = fats;
    }

    public String getName_AR() {
        return Name_AR;
    }

    public void setName_AR(String name_AR) {
        Name_AR = name_AR;
    }

    public String getCalories() {
        return Calories;
    }

    public void setCalories(String calories) {
        Calories = calories;
    }

    public String getFood_GP_ID() {
        return Food_GP_ID;
    }

    public void setFood_GP_ID(String food_GP_ID) {
        Food_GP_ID = food_GP_ID;
    }

    public String getFood_GP_Name() {
        return Food_GP_Name;
    }

    public void setFood_GP_Name(String food_GP_Name) {
        Food_GP_Name = food_GP_Name;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getDate_Time() {
        return Date_Time;
    }

    public void setDate_Time(String date_Time) {
        Date_Time = date_Time;
    }
}
