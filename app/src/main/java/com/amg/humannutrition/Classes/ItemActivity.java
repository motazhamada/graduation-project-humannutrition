package com.amg.humannutrition.Classes;

public class ItemActivity {
    private String Activities_ID;
    private String Acticity_Name;
    private String Calories_Burned;
    public String getActivities_ID() {
        return Activities_ID;
    }

    public void setActivities_ID(String activities_ID) {
        Activities_ID = activities_ID;
    }

    public String getActicity_Name() {
        return Acticity_Name;
    }

    public void setActicity_Name(String acticity_Name) {
        Acticity_Name = acticity_Name;
    }

    public String getCalories_Burned() {
        return Calories_Burned;
    }

    public void setCalories_Burned(String calories_Burned) {
        Calories_Burned = calories_Burned;
    }


}
