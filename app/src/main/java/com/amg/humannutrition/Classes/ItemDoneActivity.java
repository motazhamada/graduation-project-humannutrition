package com.amg.humannutrition.Classes;

public class ItemDoneActivity {
    private String Activities_ID;
    private String Acticity_Name;
    private String Activity_Calories_Burned;
    private String User_Activity_ID;
    private String User_ID;
    private String User_Calories_Burned;
    private String Date_Time;

    public String getUser_Activity_ID() {
        return User_Activity_ID;
    }

    public void setUser_Activity_ID(String user_Activity_ID) {
        User_Activity_ID = user_Activity_ID;
    }

    public String getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(String user_ID) {
        User_ID = user_ID;
    }

    public String getUser_Calories_Burned() {
        return User_Calories_Burned;
    }

    public void setUser_Calories_Burned(String user_Calories_Burned) {
        User_Calories_Burned = user_Calories_Burned;
    }

    public String getDate_Time() {
        return Date_Time;
    }

    public void setDate_Time(String date_Time) {
        Date_Time = date_Time;
    }

    public String getActivities_ID() {
        return Activities_ID;
    }

    public void setActivities_ID(String activities_ID) {
        Activities_ID = activities_ID;
    }

    public String getActicity_Name() {
        return Acticity_Name;
    }

    public void setActicity_Name(String acticity_Name) {
        Acticity_Name = acticity_Name;
    }

    public String getActivity_Calories_Burned() {
        return Activity_Calories_Burned;
    }

    public void setActivity_Calories_Burned(String activity_Calories_Burned) {
        Activity_Calories_Burned = activity_Calories_Burned;
    }


}
