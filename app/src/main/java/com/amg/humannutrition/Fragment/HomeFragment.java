package com.amg.humannutrition.Fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
//    // TODO: Rename parameter arguments, choose names that match
//    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private CardView bmiCardView;
    private TextView bmi, bmiType;
    private TextView freeCalories;
    private TextView freeTargetCalories, allTargetCalories;
    private TextView freeCarbs;
    private TextView freeProtein;
    private TextView freeFat;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        bmiCardView = view.findViewById(R.id.bmiHome_card);
        bmi = view.findViewById(R.id.bmiFreeHome_text);
        bmiType = view.findViewById(R.id.bmiTypeHome_text);

        freeCalories = view.findViewById(R.id.caloreisFreeHome_text);

        freeTargetCalories = view.findViewById(R.id.targetCaloreisFreeHome_text);
        allTargetCalories = view.findViewById(R.id.targetCaloreisAllHome_text);


        freeCarbs = view.findViewById(R.id.carbsFreeHome_text);

        freeProtein = view.findViewById(R.id.proteinFreeHome_text);


        freeFat = view.findViewById(R.id.fatFreeHome_text);


    }

    @Override
    public void onStart() {
        super.onStart();
        loadUserScore();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void setUserData() {
        String userJsonData = Get_UserData();
        String userJsonScore = Get_UserScore();
        try {
            JSONObject jsonUserData = new JSONObject(userJsonData);
            JSONObject jsonUserScore = new JSONObject(userJsonScore);
            bmi.setText(jsonUserData.getString("BMI"));
            double bmiDouble = Double.parseDouble(bmi.getText().toString());
            if (bmiDouble < 18.5) {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#F9F9F9"));
                bmiType.setText("Underweight");
            } else if (bmiDouble > 18.5 && bmiDouble < 24.9) {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#A1D663"));
                bmiType.setText("Healthy Weight");
            } else if (bmiDouble > 25 && bmiDouble < 29.9) {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#FFFB00"));
                bmiType.setText("Overweight");
            } else if (bmiDouble > 30 && bmiDouble < 34.9) {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#FFD300"));
                bmiType.setText("Obese");
            } else if (bmiDouble > 35 && bmiDouble < 39.9) {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#FF2600"));
                bmiType.setText("Severely Obese");
            } else {
                bmiCardView.setCardBackgroundColor(Color.parseColor("#8449B0"));
                bmiType.setText("Morbidly Obese");
            }

            freeCalories.setText(jsonUserData.getString("Calories"));

            allTargetCalories.setText(String.format("%s kcal", jsonUserData.getString("Target_Calories")));
            freeTargetCalories.setText(String.format("%s kcal", jsonUserScore.getString("Score")));


            freeCarbs.setText(String.format("%s g", jsonUserData.getString("Carbs")));

            freeProtein.setText(String.format("%s g", jsonUserData.getString("Protien")));

            freeFat.setText(String.format("%s g", jsonUserData.getString("Fats")));
        } catch (Exception e) {
            Log.e(getString(R.string.TAG),"HomeFragment 0"+ e.toString());
        }

    }

    String Get_UserData() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserData", null);
        if (restoredText != null) {
            return restoredText;
        }
        return "null";
    }

    void loadUserScore() {
        // data to populate the RecyclerView with
        try {
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Get_User_Score),
                    response -> {
                        String sResponse = String.valueOf(response);

                        if (sResponse != null && !Objects.equals(sResponse, "[]")) {
                            try {
                                Set_UserScore(sResponse);
                                setUserData();
                            } catch (Exception error) {
                                //If an error occurs that means end of the list has reached
                                Log.e(getString(R.string.TAG), "HomeFragment 1");
                            }
                        } else {

                            Log.e(getString(R.string.TAG), "HomeFragment 2 : " + response);
                        }
                    }, error -> {
                Log.e(getString(R.string.TAG), "HomeFragment 3");

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("User_ID", Get_User_ID());
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "HomeFragment 4: " + e.toString());
        }
    }

    String Get_User_ID() {
        SharedPreferences sharedPref = getContext().getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String User_ID = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_ID = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return User_ID;
        }
        return "";
    }

    private void Set_UserScore(String UserScore) {
        try {
            SharedPreferences.Editor sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.putString("JsonUserScore", UserScore);
            sharedPref.apply();
            sharedPref.commit();
        } catch (Exception e) {
//        Log.e(getString(R.string.TAG), "Error: "+e.toString());
        }
    }

    String Get_UserScore() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserScore", null);
        if (restoredText != null) {
            return restoredText;
        }
        return "null";
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
