package com.amg.humannutrition.Fragment;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.amg.humannutrition.Adapters.FoodHistoryRecyclerViewAdapter;
import com.amg.humannutrition.Classes.ItemEatenMeal;
import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NutritionHistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NutritionHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NutritionHistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private static List<ItemEatenMeal> itemEatenMeal, itemEatenMealList;

    private ProgressBar progressBar;
    private FoodHistoryRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;

    private OnFragmentInteractionListener mListener;

    public NutritionHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NutritionHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NutritionHistoryFragment newInstance(String param1, String param2) {
        NutritionHistoryFragment fragment = new NutritionHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nutrition_history, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        try {
            itemEatenMeal = new ArrayList<>();
         
            progressBar = view.findViewById(R.id.progressBar_nutrition_history);
            recyclerView = view.findViewById(R.id.recyclerView_nutrition_history);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            loadFoodHistory();
            // set up the RecyclerView
            adapter = new FoodHistoryRecyclerViewAdapter(getContext(), itemEatenMeal);
            // adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FoodHistoryFragment 1: " + e.toString());
        }
    }

    void loadFoodHistory() {
        // data to populate the RecyclerView with
        try {
            itemEatenMeal.clear();
          progressBar.setVisibility(View.VISIBLE);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(getContext());
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Get_All_Eaten_Meals),
                    response -> {
                        if (response != null && !Objects.equals(response, "[]")) {
                            try {
                                Gson gson = new Gson();
                                itemEatenMealList = gson.fromJson(response, new TypeToken<ArrayList<ItemEatenMeal>>() {
                                }.getType());
                                itemEatenMeal.addAll(itemEatenMealList);
                                adapter.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                            } catch (Exception error) {
                                progressBar.setVisibility(View.GONE);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Log.e(getString(R.string.TAG), "FoodHistoryFragment 2");
                        }
                    }, error -> {

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("User_ID", Get_User_ID());
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FoodHistoryFragment 3: " + e.toString());
        }
    }
    String Get_User_ID() {
        SharedPreferences sharedPref = getContext().getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String User_ID = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                User_ID = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return User_ID;
        }
        return "";
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
