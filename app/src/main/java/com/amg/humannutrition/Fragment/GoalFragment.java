package com.amg.humannutrition.Fragment;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.TextView;

import com.amg.humannutrition.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static com.amg.humannutrition.R.layout.custom_dialog_activity_choose;


public class GoalFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    MaterialRippleLayout heightGoalRipple_card, weightGoalRipple_card, oldWeightGoalRipple_card, goalWeightGoalRipple_card, estimatedTimeGoalRipple_card;
    TextView heightFreeGoal_text, weightFreeGoal_text, oldWeightFreeGoal_text, oldWeightDateGoal_text, goalWeightFreeGoal_text, estimatedTimeGoal_text;
   ScrollView goal_scrollView;

    private AlertDialog alertDialog = null;
    private OnFragmentInteractionListener mListener;

    public GoalFragment() {

    }

    public static GoalFragment newInstance(String param1, String param2) {
        GoalFragment fragment = new GoalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // TODO: Rename and change types of parameters
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_goal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        heightGoalRipple_card = view.findViewById(R.id.heightGoalRipple_card);
        weightGoalRipple_card = view.findViewById(R.id.weightGoalRipple_card);
        oldWeightGoalRipple_card = view.findViewById(R.id.oldWeightGoalRipple_card);
        goalWeightGoalRipple_card = view.findViewById(R.id.goalWeightGoalRipple_card);
        estimatedTimeGoalRipple_card = view.findViewById(R.id.estimatedTimeGoalRipple_card);
        heightFreeGoal_text = view.findViewById(R.id.heightFreeGoal_text);
        weightFreeGoal_text = view.findViewById(R.id.weightFreeGoal_text);
        oldWeightFreeGoal_text = view.findViewById(R.id.oldWeightFreeGoal_text);
        oldWeightDateGoal_text = view.findViewById(R.id.oldWeightDateGoal_text);
        goalWeightFreeGoal_text = view.findViewById(R.id.goalWeightFreeGoal_text);
        estimatedTimeGoal_text = view.findViewById(R.id.estimatedTimeGoal_text);
        goal_scrollView = view.findViewById(R.id.goal_scrollView);
        LoadUserData();
        heightGoalRipple_card.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            // Set icon value.
            builder.setIcon(R.drawable.ic_height_black);
            // Set title value.
            builder.setTitle("Change Height");
            View customFormView = View.inflate(view.getContext(), custom_dialog_activity_choose, null);
            builder.setView(customFormView);

            TextView message = customFormView.findViewById(R.id.textView_Dialog);
            Button cancel_action_Dialog = customFormView.findViewById(R.id.cancel_action_Dialog);
            Button choose_action_Dialog = customFormView.findViewById(R.id.choose_action_dialog);
            NumberPicker numberPicker_Dialog = customFormView.findViewById(R.id.numberPicker_Dialog);
            message.setText("Enter Your height (in cm)?");
            numberPicker_Dialog.setMinValue(50);
            numberPicker_Dialog.setMaxValue(300);
            cancel_action_Dialog.setOnClickListener(v1 -> alertDialog.cancel());
            choose_action_Dialog.setOnClickListener(v12 ->
            {
                if (SetNewData("height", numberPicker_Dialog.getValue()))
                {
                    alertDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(goal_scrollView, "Your Data changed Successfully", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else
                {
                    alertDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(goal_scrollView, "sorry there is an error please try again later", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            });
            builder.setCancelable(true);
            alertDialog = builder.create();
            alertDialog.show();
        });

        weightGoalRipple_card.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            // Set icon value.
            builder.setIcon(R.drawable.ic_weight_black);
            // Set title value.
            builder.setTitle("Change Weight");
            View customFormView = View.inflate(view.getContext(), custom_dialog_activity_choose, null);
            builder.setView(customFormView);

            TextView message = customFormView.findViewById(R.id.textView_Dialog);
            Button cancel_action_Dialog = customFormView.findViewById(R.id.cancel_action_Dialog);
            Button choose_action_Dialog = customFormView.findViewById(R.id.choose_action_dialog);
            NumberPicker numberPicker_Dialog = customFormView.findViewById(R.id.numberPicker_Dialog);
            message.setText("Enter Your Weight (in kg)?");
            numberPicker_Dialog.setMinValue(2);
            numberPicker_Dialog.setMaxValue(300);
            cancel_action_Dialog.setOnClickListener(v1 -> alertDialog.cancel());
            choose_action_Dialog.setOnClickListener(v12 ->
            {
                if (SetNewData("weight", numberPicker_Dialog.getValue()))
                {
                    alertDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(goal_scrollView, "Your Data changed Successfully", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                else
                {
                    alertDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(goal_scrollView, "sorry there is an error please try again later", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            });
            builder.setCancelable(true);
            alertDialog = builder.create();
            alertDialog.show();
        });
        goalWeightGoalRipple_card.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            // Set icon value.
            builder.setIcon(R.drawable.ic_weight_black);
            // Set title value.
            builder.setTitle("Set your Goal");
            View customFormView = View.inflate(view.getContext(), custom_dialog_activity_choose, null);
            builder.setView(customFormView);

            TextView message = customFormView.findViewById(R.id.textView_Dialog);
            Button cancel_action_Dialog = customFormView.findViewById(R.id.cancel_action_Dialog);
            Button choose_action_Dialog = customFormView.findViewById(R.id.choose_action_dialog);
            NumberPicker numberPicker_Dialog = customFormView.findViewById(R.id.numberPicker_Dialog);
            message.setText("Enter Your Goal Weight (in kg)?");
            numberPicker_Dialog.setMinValue(2);
            numberPicker_Dialog.setMaxValue(300);
            cancel_action_Dialog.setOnClickListener(v1 -> alertDialog.cancel());
            choose_action_Dialog.setOnClickListener(v12 ->
            {
                if (SetNewData("target", numberPicker_Dialog.getValue()))
            {
                alertDialog.dismiss();
                Snackbar snackbar = Snackbar.make(goal_scrollView, "Your Data changed Successfully", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            else
            {
                alertDialog.dismiss();
                Snackbar snackbar = Snackbar.make(goal_scrollView, "sorry there is an error please try again later", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
            });
            builder.setCancelable(true);
            alertDialog = builder.create();
            alertDialog.show();
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    private boolean SetNewData(String type, double value) {
        boolean Success = true;
        try {
            RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
            String url;
            switch (type) {
                case "weight":
                    url = getString(R.string.Resource_Address) + getString(R.string.Update_Weight);
                    break;
                case "height":
                    url = getString(R.string.Resource_Address) + getString(R.string.Update_Height);
                    break;
                default:
                    url = getString(R.string.Resource_Address) + getString(R.string.Update_Target_Weight);
            }
            StringRequest tokenRequest = new StringRequest(Request.Method.POST, url,
                    response -> {
                        String sResponse = String.valueOf(response);
                        if (sResponse != null && !Objects.equals(sResponse, "[]")) {
                            try {
                                JSONObject jsonObject = new JSONObject(sResponse);
                                String rEmail = jsonObject.getString("Email");
                                Set_UserData(sResponse, rEmail.toLowerCase().trim());
                                if (Check_UserData()) {
                                    LoadUserData();

                                }
                            } catch (JSONException e) {

                                Log.e(getString(R.string.TAG), "Error1: " + e.getMessage());
                            }
                        }
                    }, error -> {
                //do nothing
                Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("User_ID", Get_User_ID());
                    params.put("Value", String.valueOf(value));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(tokenRequest);
        } catch (Exception e) {
            Success = false;
            Log.e(getString(R.string.TAG), " Exception = " + String.valueOf(e));
        }
    return Success;
    }

    private void LoadUserData() {
        try {
            heightFreeGoal_text.setText(Get_User_Height());
            weightFreeGoal_text.setText(Get_User_Weight());
            goalWeightFreeGoal_text.setText(Get_User_Target_Weight());
            estimatedTimeGoal_text.setText(Get_User_Estimated_Days());
            oldWeightFreeGoal_text.setText(Get_User_Old_Weight());
            oldWeightDateGoal_text.setText(Get_User_Date_Time());

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), e.toString());
        }

    }

    String Get_User_ID() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = Objects.requireNonNull(jsonObject).getString("User_ID");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Height() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = String.format("%s cm", Objects.requireNonNull(jsonObject).getString("Height"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Weight() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = String.format("%s kg", Objects.requireNonNull(jsonObject).getString("Weight"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Old_Weight() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = String.format("%s kg", Objects.requireNonNull(jsonObject).getString("Old_Weight"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Target_Weight() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = String.format("%s kg", Objects.requireNonNull(jsonObject).getString("Target_Weight"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Estimated_Days() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value = String.format("%s Days", Objects.requireNonNull(jsonObject).getString("Estimated_Days"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }
    String Get_User_Date_Time() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String JsonUserData = sharedPref.getString("JsonUserData", null);
        String Value = "Not Defined";
        if (JsonUserData != null) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(JsonUserData);
                Value =  Objects.requireNonNull(jsonObject).getString("Date_Time");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Value;
    }

    void Set_UserData(String UserData, String Email) {
        try {
            SharedPreferences.Editor sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE).edit();
            sharedPref.putString("Email", Email);
            sharedPref.putString("JsonUserData", UserData);
            sharedPref.apply();
            sharedPref.commit();
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "Error: "+e.toString());
        }
    }

    boolean Check_UserData() {
        SharedPreferences sharedPref = Objects.requireNonNull(getContext()).getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserData", null);
        return restoredText != null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
